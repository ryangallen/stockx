# StockX

Software Engineer Coding Challenge

## Set up

This application can be run using Docker. If you cannot install Docker, a
Vagrantfile is provided for provisioning a Debian virtual machine as well.

First, clone this repository and copy the environment variables template to `.env`.

    $ git clone git@gitlab.com/ryangallen:stockx.git
    $ cd stockx
    $ cp template.env .env  # variables are preset for dev but should be changed in production

### Running with Docker

1. Install [Docker](https://docs.docker.com/install/)
   and [Docker Compose](https://docs.docker.com/compose/install/)
1. Build and run the application containers using docker-compose:

        $ docker-compose up --build -d

1. View the running application in a browser at https://localhost

### Running with Vagrant

1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and
   [Vagrant](https://www.vagrantup.com/downloads.html)
1. Add the [vagrant-docker-compose](https://github.com/leighmcculloch/vagrant-docker-compose)
   Vagrant plug-in and run the development VM:

        $ vagrant plugin install vagrant-docker-compose
        $ vagrant up

1. View the running application in a browser at https://172.16.0.2

### Running Tests

The unit tests can be run with `docker-compose exec django python manage.py test` (if you are using
Vagrant you will need to ssh into the VM first with `vagrant ssh`).

The test file is located at `/django/products/tests.py`.

### Admin user

A default admin user is set up for demonstration purposes:

**username:** root
**password:** stockXdemo

### SSL in development

**Note:** Since we are using a self-signed certificate for SSL in development,
your browser will warn that the page connection is insecure. Bypass the warning
by clicking "Advanced" and adding an exception for this certificate.

![Firefox Insecure Connection Warning](https://prod-cdn.sumo.mozilla.net/uploads/gallery/images/2018-07-24-17-48-12-79a9e2.png)

### Helpful Links

- [Docker](https://docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Vagrant](https://www.vagrantup.com/)
- [debian/contrib-stretch64](https://app.vagrantup.com/debian/boxes/contrib-stretch64)
  virtual box (`contrib-*` boxes include the `vboxfs` kernel module for shared folders)
- [Python 3.7](https://www.python.org/)
- [Pipenv](https://pipenv.readthedocs.io/en/latest/)
- [Django](https://djangoproject.com/)
- [Django Rest Framework](https://www.django-rest-framework.org/)
- [PostgreSQL](https://www.postgresql.org/)
- [nginx](https://nginx.org/en/)

## Challenge Details

A large number of sales on StockX are sneakers. However, not all sneakers fit the same (e.g.,
adidas Yeezys run a size small, therefore, if you are typically a size 9, you would buy a size 10
Yeezy). In other words, shoes are not always "true-to-size". True-to-size refers to whether or not
a shoe fits as expected for a given size. True shoe sizes are measured using the Brannock device,
the number shown on that device next to your toe is your true shoe size. What we are interested
in is letting a user know how the shoe fits based on data collected through crowdsourcing. For
this project, crowdsourcing can be you posting data to your program, repeatedly.

Your goal is to create a service, preferably written in [Go](https://golang.org), that will accept new
true-to-size data through the Hypertext Transfer Protocol (HTTP) and store this data in a relational
database, preferably Postgres. Additionally, this service should be able to return a shoe’s
`TrueToSizeCalculation`, defined below, through an HTTP request. True to size entries range between
1 and 5, inclusive, where 1: really small, 2: small, 3: true to size, 4: big and 5: really big.

    TrueToSizeCalculation = average of the true-to-size entries for a given shoe

### Example:

| **Shoe**                     |​ adidas Yeezy                             |
| ---------------------------- | ---------------------------------------- |
| **True to size data**        |​ 1, 2, 2, 3, 2, 3, 2, 2, 3, 4, 2, 5, 2, 3 |
| **True to size calculation** |​ 2.5714285714286                          |

If we add another data point, 2, to the collected data, our calculation should reflect this change.

| **Shoe**                     |​ adidas Yeezy                                |
| ---------------------------- | ------------------------------------------- |
| **True to size data**        |​ 1, 2, 2, 3, 2, 3, 2, 2, 3, 4, 2, 5, 2, 3, 2 |
| **True to size calculation** |​ 2.5333333333333                             |

### Some things to know:

- If for some reason you get stumped, feel free to email me for assistance.
- This exercise is not meant to be stressful or cut-throat so do your best.
- Do not worry about whether the data you are “crowdsourcing” is correct in the real world,
  i.e., in this example, your data might show that an adidas Yeezy fits too small. What you
  should focus on is ensuring that your program is able to calculate the appropriate true to
  size metric for an arbitrary set of true-to-size data.
- Also, this should be able to be run on any engineer’s local machine, this includes the
  database. In order to make this possible, please document the necessary steps to get
  your service and the necessary technologies running locally.
- While it is not a requirement, one way to ensure a system behaves as expected on many
  machines is to containerize the system. One technology for doing this is Docker.

If you have any questions, do not hesitate to email me at miles@stockx.com

Thanks!

Miles Muslin
