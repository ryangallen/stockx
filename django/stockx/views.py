from django.shortcuts import render
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView

from products.models import Shoe


class HomeView(View):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
        shoes = Shoe.objects.all().order_by('?')
        return render(request, self.template_name, {'shoes': shoes})


class APIHomeView(APIView):
    '''
    [Back to Site Homepage](/)

    # API Endpoints
    - [Brands](/api/brands/)
    - [Shoes](/api/shoes/)
    - [Shoe True-to-Size Values](/api/shoe-true-to-size-values/)
    '''

    def get(self, request, format=None):
        return Response()
