from rest_framework import routers

from products.views import BrandViewSet, ShoeViewSet, ShoeTrueToSizeViewSet


router = routers.DefaultRouter()
router.register(r'brands', BrandViewSet)
router.register(r'shoes', ShoeViewSet)
router.register(r'shoe-true-to-size-values', ShoeTrueToSizeViewSet)
urlpatterns = router.urls
