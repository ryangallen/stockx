from rest_framework import serializers

from products.models import Brand, Shoe, ShoeTrueToSize


class BrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = Brand
        fields = ('id', 'name',)


class ShoeSerializer(serializers.ModelSerializer):
    brand = serializers.PrimaryKeyRelatedField(queryset=Brand.objects.all())
    true_to_size_calculation = serializers.ReadOnlyField()
    true_to_size_display = serializers.ReadOnlyField()

    class Meta:
        model = Shoe
        fields = ('id', 'name', 'brand', 'true_to_size_calculation', 'true_to_size_display',)


class ShoeTrueToSizeSerializer(serializers.ModelSerializer):
    shoe = serializers.PrimaryKeyRelatedField(queryset=Shoe.objects.all())

    class Meta:
        model = ShoeTrueToSize
        fields = ('shoe', 'value',)
