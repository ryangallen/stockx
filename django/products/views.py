from rest_framework import viewsets

from products.models import Brand, Shoe, ShoeTrueToSize
from products.serializers import BrandSerializer, ShoeSerializer, ShoeTrueToSizeSerializer


class BrandViewSet(viewsets.ModelViewSet):
    '''
    ## CRUD endpoint for Brands

    ### Fields

    - **`id`** (integer) - The primary key value for the object
    - **`name`** (string) - The name of the brand
    '''
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer


class ShoeViewSet(viewsets.ModelViewSet):
    '''
    ## CRUD endpoint for Shoes

    ### Fields

    - **`id`** (integer) - The primary key value for the object
    - **`name`** (string) - The name of the shoe
    - **`brand`** (integer) - The primary key of the brand. Brands can be managed at
      [`/api/brands`](/api/brands)
    - **`true_to_size_calculation`** (float, read only) - The aggregate average of true-to-size
      values for this shoe. True-to-size values can be managed at
      [`/api/shoe-true-to-size-values`](/api/shoe-true-to-size-values)
    - **`true_to_size_display`** (string, read only) - A human-friendly description of the rounded
      true-to-size calculation
    '''

    queryset = Shoe.objects.all()
    serializer_class = ShoeSerializer


class ShoeTrueToSizeViewSet(viewsets.ModelViewSet):
    '''
    ## CRUD endpoint for Shoe True-to-Size Values

    ### Fields

    - **`shoe`** (integer) - The primary key of the shoe. Shoes can be managed at
      [`/api/shoes`](/api/shoes)
    - **`value`** (integer) - A rating, 1 (small) to 5 (big), of how the shoe fits. A value of 3 is
      considered "true to size"
    '''
    queryset = ShoeTrueToSize.objects.all()
    serializer_class = ShoeTrueToSizeSerializer
