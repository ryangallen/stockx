from django.db import models

TTS_DECIMAL_PLACES = 13
TRUE_TO_SIZE_VALUES = (
    (1, 'Really Small'),
    (2, 'Small'),
    (3, 'True to Size'),
    (4, 'Big'),
    (5, 'Really Big'),
)
TRUE_TO_SIZE_VALUES_DICT = {k: v for k, v in TRUE_TO_SIZE_VALUES}


class Entity(models.Model):
    '''Abstract base model for models with a name'''
    name = models.CharField(max_length=60)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Brand(Entity):
    pass


class Product(Entity):
    '''
    A through model for any kind of product with an associated brand
    See: https://docs.djangoproject.com/en/2.1/topics/db/models/#multi-table-inheritance
    '''
    brand = models.ForeignKey(Brand, on_delete=models.PROTECT)

    def __str__(self):
        return '{brand} {name}'.format(brand=self.brand.name, name=self.name)


class Shoe(Product):
    @property
    def true_to_size_calculation(self):
        tts_data = self.shoetruetosize_set.all()
        if tts_data:
            return round(tts_data.aggregate(models.Avg('value'))['value__avg'], TTS_DECIMAL_PLACES)

    @property
    def true_to_size_display(self):
        if self.true_to_size_calculation:
            return TRUE_TO_SIZE_VALUES_DICT[round(self.true_to_size_calculation)]


class ShoeTrueToSize(models.Model):
    shoe = models.ForeignKey(Shoe, on_delete=models.PROTECT)
    value = models.PositiveSmallIntegerField(choices=TRUE_TO_SIZE_VALUES, verbose_name="true to size value")

    class Meta:
        verbose_name = 'shoe true-to-size value'

    def __str__(self):
        return '{shoe}: {tts_value_display} ({tts_value})'.format(
            shoe=self.shoe, tts_value_display=self.get_value_display(), tts_value=self.value)
