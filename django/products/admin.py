from django.contrib import admin

from products.models import Brand, Product, Shoe, ShoeTrueToSize


class EntityAdmin(admin.ModelAdmin):
    list_display = ['name',]
    search_fields = ['name',]


@admin.register(Brand)
class BrandAdmin(EntityAdmin):
    pass


# @admin.register(Product)
class ProductAdmin(EntityAdmin):
    list_display = ['name', 'brand',]
    search_fields = ['name', 'brand__name',]
    ordering = ['brand__name', 'name',]
    list_filter = ['brand',]


@admin.register(Shoe)
class ShoeAdmin(ProductAdmin):
    list_display = ['name', 'brand', 'true_to_size_calculation', 'true_to_size_display',]
    readonly_fields = ['true_to_size_calculation', 'true_to_size_display',]


@admin.register(ShoeTrueToSize)
class ShoeTrueToSizeAdmin(admin.ModelAdmin):
    list_display = ['shoe', 'value',]
    search_fields = ['shoe__name', 'shoe__brand__name',]
    list_filter = ['value', 'shoe__brand',]
