from django.test import TestCase

from products.models import Brand, Product, Shoe, ShoeTrueToSize


class ProductsModelsTestCase(TestCase):

    def test_brand_creation(self):
        brand = Brand.objects.create(name='Nike')
        self.assertEqual(str(brand), 'Nike')

    def test_product_creation(self):
        brand = Brand.objects.create(name='Apple')
        product = Product.objects.create(brand=brand, name='iPhone XR')
        self.assertEqual(str(product), 'Apple iPhone XR')

    def test_shoe_creation(self):
        brand = Brand.objects.create(name='Adidas')
        shoe = Shoe.objects.create(brand=brand, name='Yeezy')
        self.assertEqual(str(shoe), 'Adidas Yeezy')

    def test_shoe_true_to_size_value_creation(self):
        brand = Brand.objects.create(name='Adidas')
        shoe = Shoe.objects.create(brand=brand, name='Yeezy')
        shoe_tts = ShoeTrueToSize.objects.create(shoe=shoe, value=4)
        self.assertEqual(str(shoe_tts), 'Adidas Yeezy: Big (4)')
        self.assertEqual(shoe_tts.get_value_display(), 'Big')

    def test_shoe_true_to_size_calculation_and_display(self):
        brand = Brand.objects.create(name='Adidas')
        shoe = Shoe.objects.create(brand=brand, name='Yeezy')

        # TrueToSizeCalculation = average of the true to size entries for a given shoe
        for value in [1, 2, 2, 3, 2, 3, 2, 2, 3, 4, 2, 5, 2, 3]:
            ShoeTrueToSize.objects.create(shoe=shoe, value=value)
        self.assertEqual(shoe.true_to_size_calculation, 2.5714285714286)
        self.assertEqual(shoe.true_to_size_display, 'True to Size')

        # If we add another data point, our calculation should reflect this change
        ShoeTrueToSize.objects.create(shoe=shoe, value=2)
        self.assertEqual(shoe.true_to_size_calculation, 2.5333333333333 )
        self.assertEqual(shoe.true_to_size_display, 'True to Size')

        # True-to-size display changes based on rounded TTS calculation
        ShoeTrueToSize.objects.create(shoe=shoe, value=1)
        self.assertEqual(shoe.true_to_size_calculation, 2.4375)
        self.assertEqual(shoe.true_to_size_display, 'Small')
